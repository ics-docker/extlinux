FROM debian:9

LABEL maintainer "benjamin.bertrand@esss.se"

RUN apt-get update \
  && apt-get install -y \
    curl \
    syslinux \
    extlinux \
    parted \
    e2fsprogs \
    udev \
  && rm -rf /var/lib/apt/lists/*
