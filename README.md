# extlinux

[Docker](https://www.docker.com) Debian image with [extlinux](https://www.syslinux.org/wiki/index.php?title=EXTLINUX) pre-installed.

Debian is used to have a recent version of syslinux (6).
CentOS comes with version 4. Trying to boot on a CentOS 7.6 kernel fails.

To use this image to create a bootable USB key, you have to run in privileged mode and mount the /dev directory:

```
docker run --rm -it -v /dev:/dev --privileged registry.esss.lu.se/ics-docker/extlinux
```
